/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.absanimal;

/**
 *
 * @author WIP
 */
public abstract class AquaticAnm extends Animal {
    
    public AquaticAnm(String nickname) {
        super(nickname, 0);
    }
    
    public abstract void swim();
    
}
