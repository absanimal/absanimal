/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.absanimal;

/**
 *
 * @author WIP
 */
public class TestAnimal {
    public static void main(String[] args) {
        //Human can run
        Human h1 = new Human("James");
        h1.eat();
        h1.walk();
        h1.run();
        System.out.println("h1 is animal? " + (h1 instanceof Animal));
        System.out.println("h1 is animal? " + (h1 instanceof LandAnm));
        
         Animal a1 = h1;
         System.out.println("a1 is animal? " + (a1 instanceof Animal));
         System.out.println("a1 is animal? " + (a1 instanceof Reptile));
         System.out.println("");
         
         //Cat 
         Cat c1 = new Cat("Realm");
         c1.eat();
         c1.walk();
         c1.run();
         System.out.println("c1 is animal? " + (c1 instanceof Animal));
         System.out.println("c1 is animal? " + (c1 instanceof LandAnm));
         
         Animal a2 = c1;
         System.out.println("a2 is animal? " + (a2 instanceof LandAnm));
         System.out.println("a2 is animal? " + (a2 instanceof AquaticAnm));
         System.out.println("a2 is animal? " + (a2 instanceof Animal));
         System.out.println("");
         
         //Dog
         Dog d1 = new Dog("Miki");
         d1.run();
         d1.speak();
         System.out.println("d1 is animal? " + (d1 instanceof Animal));
         System.out.println("d1 is animal? " + (d1 instanceof LandAnm));
         
         Animal a3 = d1;
         System.out.println("a3 is animal? " + (a3 instanceof Animal));
         System.out.println("a3 is animal? " + (a3 instanceof Poultry));
         System.out.println("");
         
         //Crocodile can crawl
         Crocodile cd1 = new Crocodile("Gabiru");
         cd1.crawl();
         cd1.eat();
         System.out.println("cd1 is animal? " + (cd1 instanceof Animal));
         System.out.println("cd1 is animal? " + (cd1 instanceof Reptile));
         
         Animal a4 = cd1;
         System.out.println("a4 is animal? " + (a4 instanceof LandAnm));
         System.out.println("a4 is animal? " + (a4 instanceof AquaticAnm));
         System.out.println("");
         
         //Snake
         Snake s1 = new Snake("Albis");
         s1.sleep();
         s1.crawl();
         System.out.println("s1 is animal? " + (s1 instanceof Animal));
         System.out.println("s1 is animal? " + (s1 instanceof Reptile));
         
         Animal a5 = s1;
         System.out.println("a5 is animal? " + (a5 instanceof LandAnm));
         System.out.println("a5 is animal? " + (a5 instanceof Poultry));
         System.out.println("a5 is animal? " + (a5 instanceof Reptile));
         System.out.println("");
         
         //Fish can swim
         Fish f1 = new Fish("Marlin");
         f1.swim();
         f1.walk();
         System.out.println("f1 is animal? " + (f1 instanceof Animal));
         System.out.println("f1 is animal? " + (f1 instanceof AquaticAnm));
         
         Animal a6 = f1;
         System.out.println("a6 is animal? " + (a6 instanceof LandAnm));
         System.out.println("a6 is animal? " + (a6 instanceof Poultry));
         System.out.println("a6 is animal? " + (a6 instanceof Animal));
         System.out.println("");
         
         //Crab
         Crab cb1 = new Crab("Cancer");
         cb1.swim();
         cb1.speak();
         cb1.walk();
         System.out.println("cb1 is animal? " + (cb1 instanceof AquaticAnm));
         
         Animal a7 = cb1;
         System.out.println("a7 is animal? " + (a7 instanceof Reptile));
         System.out.println("a7 is animal? " + (a7 instanceof Animal));
         System.out.println("");
         
         //Bat can fly
         Bat b1 = new Bat("Valentine");
         b1.fly();
         b1.speak();
         System.out.println("b1 is animal? " + (b1 instanceof Poultry));
         
         Animal a8 = b1;
         System.out.println("a8 is animal? " + (a8 instanceof Reptile));
         System.out.println("a8 is animal? " + (a8 instanceof LandAnm));
         System.out.println("");
         
         //Bird
         Bird bd1 = new Bird("Veldora");
         bd1.fly();
         bd1.walk();
         System.out.println("bd1 is animal? " + (bd1 instanceof Poultry));
         System.out.println("bd1 is animal? " + (bd1 instanceof Animal));
         
         Animal a9 = bd1;
         System.out.println("a9 is animal? " + (a9 instanceof AquaticAnm));
         System.out.println("a9 is animal? " + (a9 instanceof LandAnm));
         System.out.println("a9 is animal? " + (a9 instanceof Animal));
         
    }
}
