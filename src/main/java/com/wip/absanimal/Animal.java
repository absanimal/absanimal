/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.absanimal;

/**
 *
 * @author WIP
 */
public abstract class Animal {
    private String nickname;
    private int numberOfLeg;

    public Animal(String nickname, int numberOfLeg) {
        this.nickname = nickname;
        this.numberOfLeg = numberOfLeg;
    }

    public String getNickname() {
        return nickname;
    }

    public int getNumberOfLeg() {
        return numberOfLeg;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setNumberOfLeg(int numberOfLeg) {
        this.numberOfLeg = numberOfLeg;
    }

    @Override
    public String toString() {
        return "Animal{" + "nickname=" + nickname + ", numberOfLeg=" + numberOfLeg + '}';
    }

    public abstract void eat();

    public abstract void walk();

    public abstract void speak();

    public abstract void sleep();
}
