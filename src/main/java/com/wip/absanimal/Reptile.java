/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.absanimal;

/**
 *
 * @author WIP
 */
public abstract class Reptile extends Animal {
    
    public Reptile(String nickname, int numberOfLeg) {
        super(nickname, numberOfLeg);
    }
    
    public abstract void crawl();
    
}
